import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    titles = ['Mr.', 'Mrs.', 'Miss.']
    result = []

    for title in titles:
        filtered_data = df[df['Name'].str.contains(title)]
        missing_values = filtered_data['Age'].isnull().sum()
        median_age = filtered_data['Age'].median()
        rounded_median_age = round(median_age)
        result.append((title, missing_values, rounded_median_age))

    return result
